import  { API_REQUEST_SUCCESS, API_REQUEST_ERROR} from '../actions/frame-details-action'

export default function frameDetailsReducer(state = {}, action) {
    console.log('actions', action);
    switch (action.type) {
        case API_REQUEST_SUCCESS:
            return action.payload.frameDetails;
        case API_REQUEST_ERROR:
            return action.payload.frameDetails;
        default:
            return state;
    }
}