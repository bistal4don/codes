
import  { API_REQUEST_SUCCESS, API_REQUEST_ERROR} from '../actions/picture-action'

export default function picturesReducer(state = [], action) {
    switch (action.type) {
        case API_REQUEST_SUCCESS:
            return action.payload.pictures;
        case API_REQUEST_ERROR:
            return action.payload.pictures;
        default:
            return state;
    }
}