import React, { Component } from "react";
import "./App.css";
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { updatePicturesAction, apiRequestPictures } from "../actions/picture-action";
import { updateFrameDetailsAction, apiRequestFrameDetail } from "../actions/frame-details-action";
import { WebPage } from "../components/page/page-layout"; 

class App extends Component {

  componentDidMount() {
    this.props.onApiRequestPictures();  
  }

  shouldComponentUpdate (){
    return true;
  }

  componentDidUpdate(prevSate) {
     if( prevSate.pictures !== this.props.pictures) {
       this.props.onApiRequestFrameDetail(this.props.pictures, this);
     }
  }

  search = e => {
    if (e.target.value.length > 2) {
    this.props.onApiRequestPictures(e.target.value);

    }
  };

  delay = (function() {
    var timer = 0;
    return function(callback, ms) {
      clearTimeout(timer);
      timer = setTimeout(callback, ms);
    };
  })();

  render() {

    if (this.props.frameDetails.length !== 0) {
      return (
        <div className="App">
          <input type="text"onChange={this.search} placeholder="Enter a value to search"/>
          <WebPage className="webPage" pictures={this.props.pictures} frameDetails={this.props.frameDetails}/>

        </div>
      );
    } else {
      return null;
    }
  }
}

const picturesSelector = createSelector(
    state => state.pictures,
    pictures => pictures
);

const frameDetailsSelector = createSelector(
    state => state.frameDetails,
    frameDetails => frameDetails
);

const detailArraySelector = createSelector(
    state => state.detailArray,
    detailArray => detailArray
);

const mapStateToProps = createSelector(
  picturesSelector,
  frameDetailsSelector,
      (pictures, frameDetails) => ({
        pictures : pictures,
        frameDetails: frameDetails, 
        detailArray: detailArraySelector
      })
);


const mapActionsToProps = {
    onUpdateFrameDetails: updateFrameDetailsAction,
    onUpdatePictures: updatePicturesAction,
    onApiRequestFrameDetail: apiRequestFrameDetail,
    onApiRequestPictures: apiRequestPictures
};

export default connect(mapStateToProps, mapActionsToProps)(App);
