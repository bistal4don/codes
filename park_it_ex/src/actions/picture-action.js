import {KEY }from "../API_KEY"

export const API_REQUEST_SUCCESS = 'pictures:updatepictures';
export const API_REQUEST_ERROR = 'pictures:showError';
export const API_REQUEST_REQUEST = 'pictures:showRequest';


//var KEY = '909222a4cba6531dc368d756ecfa3857';

export function apiRequestPictures(searchValue = '') {
    console.log(searchValue);
    return (dispatch) => {
    fetch("https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=" + KEY +
    "&tags=safe,good&text=" +
    searchValue +
    "&format=json&nojsoncallback=1"
)
    .then( function(response) {
        return response.json();
      })
      .then(j => {

        let picArray = j.photos.photo.map(pic => {
            var srcPath = "https://farm" + pic.farm + ".staticflickr.com/" + pic.server + "/" + pic.id + "_" +
              pic.secret +  ".jpg";
            var all = { url: srcPath, id:  pic.id}  
            return all;
          });

          if(j.stat === 'ok') {
              console.log('SUCCESS');
              dispatch(updatePicturesAction(picArray))
          }else {
              console.log('ERROR');
              dispatch(showError());
          }
      })
    }
}

export function showError() {
    return {
        type: API_REQUEST_ERROR,
        payload: {
           pictures: { tags: 'ERROR !!'}
        }
    }
}

export function onSearch(parameter) {
    return {
        type: API_REQUEST_REQUEST,
        // payload: {
        //    frameDetails: { tags: 'SUCCESS !!'}
        // }
    }
}

export function  updatePicturesAction(newPictures) {
    return {
        type: API_REQUEST_SUCCESS,
        payload: {
            pictures: newPictures
        }
    }
}



