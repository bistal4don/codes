import {KEY }from "../API_KEY"

export const API_REQUEST_SUCCESS = "frameDetails:updateFrameDetails";
export const API_REQUEST_ERROR = "frameDetails:showError";
export const API_REQUEST_REQUEST = "frameDetails:showRequest";

export function apiRequestFrameDetail(obj) {
  return dispatch => {
    obj.forEach(elem => {
      fetch(
        "https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=" +
          KEY +
          "&photo_id=" +
          elem.id +
          "&format=json&nojsoncallback=1",
        { mode: "cors" }
      )
        .then(function(response) {
          return response.json();
        })
        .then(pdata => {
          if (pdata.stat === "ok") {
            console.log("SUCCESS");

            pdata = pdata.photo;

            let allTags = pdata.tags.tag.map(tg => {
              return tg.raw + ", ";
            });

            var allDetails = {
              photo: elem.url,
              title: pdata.title._content,
              author: pdata.owner.realname,
              authorPage: pdata.owner.nsid,
              description: pdata.description._content,
              tags: allTags,
              pixId: elem.id,
              status: true
            };

            makeArray(allDetails, obj, dispatch);
          } else {
            console.log("ERROR");
            dispatch(showError());
          }
        });
    });
  };
}

let arr = [];

export function makeArray(val, obj, dispatch) {
  arr = arr.concat(val);

  if (obj.length === arr.length) {
    dispatch(updateFrameDetailsAction(arr));
  }
}

export function showError() {
  return {
    type: API_REQUEST_ERROR,
    payload: {
      frameDetails: { tags: "ERROR !!" }
    }
  };
}

export function updateFrameDetailsAction(newFrameDetails) {
  return {
    type: API_REQUEST_SUCCESS,
    payload: {
      frameDetails: newFrameDetails
    }
  };
}
