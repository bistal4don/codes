import React, { Component } from "react";
import { Panel } from "react-bootstrap";
import "./detail-frame.css";

export class DetailFrame extends Component {
  componentDidUpdate() {}

  render() {
    return (
      <div className="detailFrame">
        <Panel.Heading className="pnlAuthor">
          <a href={"https://www.flickr.com/photos/" + this.props.authorPage}>
            {this.props.author}
          </a>
        </Panel.Heading>
        <div className="by">by</div>

        <Panel.Title className="pnlTitle">
          <a
            href={
              "https://www.flickr.com/photos/" +
              this.props.authorPage +
              "/" +
              this.props.pixId
            }
          >
            {this.props.title}
          </a>
        </Panel.Title>

        <div className="desTitle">
          <b>Desciption: </b>
        </div>
        <Panel.Body className="pnlDescription">
          {this.props.description}
        </Panel.Body>

        <Panel.Footer className="pnlFooter">
          <b>Tags: </b>
          {this.props.tags}
        </Panel.Footer>
      </div>
    );
  }
}