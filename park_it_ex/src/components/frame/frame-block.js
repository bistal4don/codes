import React, { Component } from "react";
import { Panel } from "react-bootstrap";
import { PictureFrame } from "../picture/picture-frame"; 
import { DetailFrame } from "../detail/detail-frame"; 
import "./frame-block.css"


export class FrameBlock extends Component {
  shouldComponentUpdate(nextProps) {
    return true;
  }

  render() {
    if (this.props.details) {
      return (
        <div>
          <Panel className="frameBlock">
            <Panel.Body>
              {" "}
              <PictureFrame value={this.props.pics.url} />
            </Panel.Body>
            <DetailFrame
              tags={this.props.details.tags}
              title={this.props.details.title}
              author={this.props.details.author}
              description={this.props.details.description}
              authorPage={this.props.details.authorPage}
              pixId={this.props.details.pixId}
            />
          </Panel>
        </div>
      );
    } else{
      return null;
    }
  }
}