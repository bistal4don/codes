import React, { Component } from "react";
import "./page-layout.css"
import { FrameBlock } from "../frame/frame-block";

export class WebPage extends Component {
  static storeDetails = [];

  constructor(props) {
    super(props);
    this.state = {
      newVal: {},
      allFrames: [],
      isLoaded: false,
      index: 0
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.pictures !== this.props.pictures) {
      this.loadPage();
    }
  }

  clearPage = () => {
    for (let i = 0; i < this.props.length; i++) {
      this.setState({ newVal: [], isLoaded: false, index: 0 });
    }
  };

  loadPage = () => {};

  render() {
    var allNewFrames = [];
    if (this.props.frameDetails) {
      for (let i = 0; i < this.props.pictures.length; i++) {
        allNewFrames.push(
          <FrameBlock
            pics={this.props.pictures[i]}
            details={this.props.frameDetails[i]}
            key={i}
          />
        );
      }
    }
    return <div>{allNewFrames}</div>;
  }
}
