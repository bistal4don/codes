import React, { Component } from "react";
import { Image } from "react-bootstrap";
import "./picture-frame.css";

export class PictureFrame extends Component {
    render() {
      return (
        <div className="picsFrame">
          <Image
            alt="dogs"
            className="standardPixSize"
            src={this.props.value}
            responsive
          />
        </div>
      );
    }
  }