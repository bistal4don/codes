import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './container/App';
import * as serviceWorker from './serviceWorker';
import pictureReducer from './reducers/picture-reducer';
import  frameDetailsReducer from './reducers/frame-details-reducer';

import  thunk  from 'redux-thunk';
import { applyMiddleware, compose, combineReducers, createStore } from 'redux';
import { Provider } from 'react-redux';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const allReducers = combineReducers({
    pictures: pictureReducer,
    frameDetails: frameDetailsReducer
})

const allAppEnhancers = compose(
    composeEnhancers(applyMiddleware(thunk)),
    // window.devToolsExtension && window.devToolsExtension())
);

const store = createStore(
    allReducers, 
    {
        pictures: [{ url: 'www.flickr.com'}],
        frameDetails: []
    },
    allAppEnhancers
    );

ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
